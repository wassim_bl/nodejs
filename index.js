const express = require("express")
const app = express()

app.use(express.json())

const equipes = require("./equipes.json")
const joueurs = require("./joueurs.json")

app.listen(82,()=>{
    console.log("Rest api via express JS")
})

//equipes
    app.get("/equipes",(req,res) => {
        //res.send("liste des equipes .")
        res.status(200).json(equipes)
    })

    app.get("/equipes/:id",(req,res) => {
        const id =parseInt(req.params.id)
        const equipe = equipes.find(equipe => equipe.id === id)
        res.status(200).json(equipe)
    })

    app.post("/equipes",(req,res) => {
        equipes.push(req.body)
        res.status(200).json(equipes)
    })
    app.put("/equipes/:id" ,(req,res)=>{
        const id = parseInt(req.params.id)
        let equipe =equipes.find(equipe=>equipe.id === id)
        equipe.name = req.body.name,
        equipe.country = req.body.country,
        res.status(200).json(equipe)
    })
    app.delete("/equipes/:id" ,(req,res)=>{
        const id = parseInt(req.params.id)
        let equipe =equipes.find(equipe=>equipe.id === id)
        equipes.splice(equipes.indexOf(equipe),1)
        res.status(200).json(equipe)
    })

//joueurs
// 1--------------------------------------------------------
    app.get("/joueurs",(req,res) => {
        res.status(200).json(joueurs)
    })

    app.get("/joueurs/:id",(req,res) => {
        const id =parseInt(req.params.id)
        const joueur = joueurs.find(joueur => joueur.id === id)
        res.status(200).json(joueur)
    })

    app.post("/joueurs",(req,res) => {
        joueurs.push(req.body)
        res.status(200).json(joueurs)
    })
    app.put("/joueurs/:id" ,(req,res)=>{
        const id = parseInt(req.params.id)
        let joueur =joueurs.find(joueur=>joueur.id === id)
        joueur.name = req.body.name,
        joueur.country = req.body.country,
        res.status(200).json(joueur)
    })
    app.delete("/joueurs/:id" ,(req,res)=>{
        const id = parseInt(req.params.id)
        let joueur =joueurs.find(joueur=>joueur.id === id)
        joueurs.splice(joueurs.indexOf(joueur),1)
        res.status(200).json(joueur)
    })

// 2----------------------------------------------------------------

    app.get("/joueurs/equipes/:id",(req,res)=>{
        const idequipe=parseInt(req.params.id);
        const resultat=[]
        for (let i=0;i<joueurs.length;i++){
            if(joueurs[i].idEquipe === idequipe){
                resultat.push(joueurs[i])
            }
        }
        res.status(200).json(resultat)
    })

// 3--------------------------------------------------------

    app.get("/equipes/joueurs/:id",(req,res)=>{
        const idJoueur=parseInt(req.params.id);
        const resultat=[]
        for (let i=0;i<equipes.length;i++){
            if(equipes[i].id === idJoueur){
                resultat.push(equipes[i])
            }
        }
        res.status(200).json(resultat)
    })

// 4--------------------------------------------------------

    app.get("/joueur/:nom",(req,res)=>{
        const nomJoueur=req.params.nom.toLowerCase();
        const resultat=[]
        for (let i=0;i<joueurs.length;i++){
            if(joueurs[i].nom === nomJoueur){
                resultat.push(joueurs[i])
            }
        }
        res.status(200).json(resultat)
    })




